
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nick
 */
public class FindFrenchURL {

    final String urlpt1 = "http://nlds", urlpt2;
    String[] cache;
    ArrayList<String> d = new ArrayList<>();
    int c = 0;

    public FindFrenchURL(String team, int type) {
        if (type == 0) {
            urlpt2 = ".cdnak.neulion.com/nlds/nhl/" + team + "/as/live/" + team + "_hd_ipad.m3u8";
        } else {
            urlpt2 = ".cdnak.neulion.com/nlds/nhlfr/" + team + "/as/live/" + team + "_hd_ipad.m3u8";
        }
    }

    public String[] FindURL() throws IOException, InterruptedException {
        Thread thread = new Thread() {
            @Override
            public void run() {

                for (int i = 1; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread2 = new Thread() {
            @Override
            public void run() {

                for (int i = 2; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread3 = new Thread() {
            @Override
            public void run() {

                for (int i = 3; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread4 = new Thread() {
            @Override
            public void run() {

                for (int i = 4; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };
        Thread thread5 = new Thread() {
            @Override
            public void run() {

                for (int i = 5; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };
        Thread thread6 = new Thread() {
            @Override
            public void run() {

                for (int i = 6; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread7 = new Thread() {
            @Override
            public void run() {

                for (int i = 7; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread8 = new Thread() {
            @Override
            public void run() {

                for (int i = 8; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };

        Thread thread9 = new Thread() {
            @Override
            public void run() {

                for (int i = 9; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        
                    }
                }
            }
        };
        Thread thread10 = new Thread() {
            @Override
            public void run() {

                for (int i = 10; i < 300; i = i + 10) {

                    
                    if (fetchURL(urlpt1 + i + urlpt2)) {
                        System.out.println(i);
                        d.add(urlpt1 + i + urlpt2);
                        c++;
                    }
                }
            }
        };
        thread.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();
        thread7.start();
        thread8.start();
        thread9.start();
        thread10.start();

        thread.join();
        thread2.join();
        thread3.join();
        thread4.join();
        thread5.join();
        thread6.join();
        thread7.join();
        thread8.join();
        thread9.join();
        thread10.join();

        Object[] tmp = d.toArray();
        cache = Arrays.copyOf(tmp, tmp.length, String[].class);
        
        return cache;
    }

    private boolean fetchURL(String testURL) {
        try {
            URL u = new URL(testURL);
            HttpURLConnection huc = (HttpURLConnection) u.openConnection();
            huc.setRequestMethod("HEAD");
            huc.setReadTimeout(1000);
            huc.connect();
            int code = huc.getResponseCode();

            return code == 200 || code == 403;
        } catch (MalformedURLException ex) {
            return false;
        } catch (IOException ex) {
            return false;
        }
    }

    public String[] findRightStream(String[] testURL) {
        String[] good = new String[3];

        URL u;
        int code, i = 0;
        System.out.println(testURL.length);
        for (int j = 0; j < testURL.length; j++) {
            try {
                u = new URL(testURL[j]);
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("HEAD");
                huc.setReadTimeout(1000);
                huc.setRequestProperty("http.agent", "PS4 libhttp/1.76 (PlayStation 4)");
                huc.connect();
                code = huc.getResponseCode();
                
                if (code == 200) {
                    good[i] = testURL[j];
                    System.out.println("Adding http200 link");
                    i++;
                }
            } catch (MalformedURLException ex) {
                System.err.println(ex);
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }

        return good;
    }

}

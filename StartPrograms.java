import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StartPrograms {

    private final String os;

    public StartPrograms(String opsys) {
        os = opsys;
    }

    public ProcessBuilder StartJAR(String loc, String gid, String ha, String pw) throws IOException {
        ProcessBuilder jar;

        if (os.toLowerCase().contains("win")) {
            jar = new ProcessBuilder("java", "-jar", loc, gid, ha);
        } else {
            String p = "echo \'" + pw + "\' | ";
            jar = new ProcessBuilder("/bin/sh", "-c", p + "sudo -S java -jar " + loc + " " + gid + " " + ha);
        }

        return jar;
    }

    public ProcessBuilder StartVLC(String loc, String arg) throws IOException {
        ProcessBuilder vlc;
        
        if (os.toLowerCase().contains("win")) {
            String a1=arg.split(" ")[0],a2;
            try {
                a2=arg.split(" ")[1];
            }
            catch (ArrayIndexOutOfBoundsException u) {
                a2="";
            }
            vlc = new ProcessBuilder("\"" + loc + "\"", a1, a2);
        } else {
            vlc = new ProcessBuilder("/bin/bash", "-c", "\"" + loc + "\" " + arg);
        }

        return vlc;
    }

    public void editHost(String pw) throws IOException {
        String p = "echo \'" + pw + "\' | sudo -S ";
        if (os.contains("mac")) {
            new ProcessBuilder("/bin/sh", "-c", p + "sed -i \"3i127.0.0.1 nlsk.neulion.com\" /private/etc/hosts").start();
        } else {
            new ProcessBuilder("/bin/sh", "-c", p + "sed -i \"3i127.0.0.1 nlsk.neulion.com\" /etc/hosts").start();
        }

    }

    public static void killJAR(String pw) throws IOException {
        String buf, b = "";
        String p = "echo \'" + pw + "\' | sudo -S ";
        Process pb = new ProcessBuilder("/bin/sh", "-c", p + "lsof -i :80").start();
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(pb.getInputStream()));
        int i = 0;

        while ((buf = stdInput.readLine()) != null) {
            if (i >= 1) {
                if (buf.contains("java") && buf.contains("root")) {
                    b = buf;
                    break;
                }
            }
            i++;
        }
        if (!b.equals("")) {
            int j = b.indexOf("java"), r = b.indexOf("root");

            String pid = b.substring(j + 4, r);
            pid = pid.replace("\t", "").replace(" ", "");
            System.out.println(pid);

            new ProcessBuilder("/bin/sh", "-c", p + "kill " + pid).start();
        }
    }
}


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Nick
 */
public class Timezone {

    public Timezone() {

    }

    public String getPSTDate() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMdd");

        df.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));

        return df.format(today);
    }

    public String toLocalTZ(String time) throws ParseException {
        TimeZone timeZone1 = TimeZone.getTimeZone("America/New_York");
        TimeZone timeZone2 = TimeZone.getDefault();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd H:mm"); // 12 hour format
        SimpleDateFormat l = new SimpleDateFormat("yyyMMdd h:mm a"); // 12 hour format

        format.setTimeZone(timeZone1);
        java.util.Date d1 = (java.util.Date) format.parse(time);

        l.setTimeZone(timeZone2);
        String s = format.format(d1);

        d1 = (java.util.Date) format.parse(s);
        return l.format(d1);

    }

    public boolean compareTime(String t1, String t2) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMd h:mm"); // 12 hour format
        java.util.Date d1 = (java.util.Date) format.parse(t1);
        java.util.Date d2 = (java.util.Date) format.parse(t2);

        return d1.after(d2);

    }

    public boolean compareTime2(String t1, String t2) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMd h:mm a"); // 12 hour format
        java.util.Date d1 = (java.util.Date) format.parse(t1);
        java.util.Date d2 = (java.util.Date) format.parse(t2);

        Calendar cal = Calendar.getInstance();
        cal.setTime(d2);
        cal.add(Calendar.MINUTE, -20);
        
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d1);
        long d = cal2.getTimeInMillis(), d3 = cal.getTimeInMillis();
        
        return d<=d3;

    }

    public String getDate() {
        Date today = new Date();
        DateFormat df = new SimpleDateFormat("yyyyMMdd h:mm a");

        return df.format(today);
    }

    public String getPSTDate(Calendar text) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        java.util.Date d1 = text.getTime();
        
        return df.format(d1);
        
    }
}
